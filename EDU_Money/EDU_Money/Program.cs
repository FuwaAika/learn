﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using EDU_Money.Entities;

namespace EDU_Money
{
    class Program
    {
        static void Main(string[] args)
        {
            var currencyRates = new List<CurrencyRate>
            {
                new CurrencyRate(Currency.EUR, 89.90m),
                new CurrencyRate(Currency.USD, 76.16m),
                new CurrencyRate(Currency.RUB, 1m),
                new CurrencyRate(Currency.JPY, 0.70m)
            };

            MainDialog(currencyRates);
        }

        public static Money ParseMoneyDialog(List<CurrencyRate> currencyRates)
        {
            Console.WriteLine("Please type Money:");
            var incomingString = Console.ReadLine();
            var result = Money.TryParse(incomingString, currencyRates, out var money);
            if (result) return money;
            Console.WriteLine("Wrong format. Please try again!");
            return ParseMoneyDialog(currencyRates);
        }

        public static void MainDialog(List<CurrencyRate> currencyRates)
        {
            Console.WriteLine("Available format {Amount}{Currency Symbol} or {Amount} {Currency Symbol}.\nAvailable currency symbols: $, ₽, £, ¥, R, E, U, Y.\nAvailable separator: .");
            var moneyLeft = ParseMoneyDialog(currencyRates);
            var moneyRight = ParseMoneyDialog(currencyRates);

            if (moneyLeft > moneyRight)
            {
                Console.WriteLine($"{moneyLeft} > {moneyRight}");
            }

            if (moneyLeft < moneyRight)
            {
                Console.WriteLine($"{moneyRight} > {moneyLeft}");
            }

            if (moneyLeft == moneyRight)
            {
                Console.WriteLine($"{moneyLeft} is equal to {moneyRight}");
            }

            Console.WriteLine("Try again? Y/N");

            if (Console.ReadLine() == "Y")
            {
                MainDialog(currencyRates);
            }
        }
    }
}
