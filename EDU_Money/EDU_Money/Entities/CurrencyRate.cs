﻿namespace EDU_Money.Entities
{
    public class CurrencyRate
    {
        private Currency _currency { get; }
        private decimal _currencyMultiplier { get; }
        public Currency Currency => _currency;
        public decimal CurrencyMulitiplier => _currencyMultiplier;

        public CurrencyRate(Currency currency, decimal currencyMultiplier)
        {
            _currency = currency;
            _currencyMultiplier = currencyMultiplier;
        }
    }
}
