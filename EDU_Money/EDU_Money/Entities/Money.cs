﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using EDU_Money.Entities;
using EDU_Money.Extensions;

namespace EDU_Money
{
    public class Money : IEquatable<Money>, IComparable<Money>
    {
        private decimal _amount { get; }
        private CurrencyRate _currency { get; }

        public decimal Amount => _amount;
        public CurrencyRate Currency => _currency;

        public Money(decimal amount, CurrencyRate currency)
        {
            _amount = amount;
            _currency = currency;
        }

        public override string ToString()
        {
            return $"{_amount} {_currency.Currency}";
        }

        public static bool TryParse(string value, List<CurrencyRate> currentRates, out Money money)
        {
            try
            {
                var currencyFormater = new NumberFormatInfo
                {
                    NegativeSign = "-",
                    CurrencyDecimalSeparator = ".",
                    CurrencySymbol = "$"
                };
                var regex = new Regex(BuildRegexString(value));
                if (regex.IsMatch(value))
                {
                    var currencyRate = currentRates.First(cr => cr.Currency == value.Last().ToString().ToCurrency());
                    value = value.Remove(value.Length - 1, 1).Trim(' ');
                    var amount = decimal.Parse(value, NumberStyles.Currency, currencyFormater);
                    if (currencyRate != null)
                    {
                        money = new Money(amount, currencyRate);
                        return true;
                    }
                }

                money = null;
                return false;
            }
            catch
            {
                money = null;
                return false;
            }
        }

        public static Money operator + (Money left, Money right)
        {
            if(left is null || right is null) throw new InvalidOperationException("null is invalid!");
            return new Money(
                (left._amount * left._currency.CurrencyMulitiplier +
                 right._amount * right._currency.CurrencyMulitiplier) / left._currency.CurrencyMulitiplier,
                left._currency);
        }

        public static Money operator - (Money left, Money right)
        {
            if (left is null || right is null) throw new InvalidOperationException("null is invalid!");
            return new Money((left._amount * left._currency.CurrencyMulitiplier -
                               right._amount * right._currency.CurrencyMulitiplier) / left._currency.CurrencyMulitiplier,
                left._currency);
        }

        public static bool operator > (Money left, Money right)
        {
            if (left is null || right is null) return false;
            return left._amount * left._currency.CurrencyMulitiplier >
                   right._amount * right._currency.CurrencyMulitiplier;
        }

        public static bool operator < (Money left, Money right)
        {
            if (left is null || right is null) return false;
            return left._amount * left._currency.CurrencyMulitiplier <
                   right._amount * right._currency.CurrencyMulitiplier;
        }

        public static bool operator == (Money left, Money right)
        {
            if (left is null || right is null) return false;
            return left._amount * left._currency.CurrencyMulitiplier ==
                   right._amount * right._currency.CurrencyMulitiplier;
        }

        public static bool operator != (Money left, Money right)
        {
            if (left is null || right is null) return false;
            return left._amount * left._currency.CurrencyMulitiplier !=
                   right._amount * right._currency.CurrencyMulitiplier;
        }

        public static bool operator >= (Money left, Money right)
        {
            return (left > right) || (left == right);
        }

        public static bool operator <= (Money left, Money right)
        {
            return (left < right) || (left == right);
        }

        public bool Equals(Money other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return _amount == other._amount && Equals(_currency, other._currency);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Money) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (_amount.GetHashCode() * 397) ^ (_currency != null ? _currency.GetHashCode() : 0);
            }
        }

        public int CompareTo(Money other)
        {
            if (ReferenceEquals(this, other)) return 0;
            if (ReferenceEquals(null, other)) return 1;
            return (_amount * _currency.CurrencyMulitiplier).CompareTo(
                other._amount * other._currency.CurrencyMulitiplier);
        }

        private static string BuildRegexString(string value)
        {
            var defaultRegexString = "\\d*";
            if (value.IndexOf('.') > 0)
            {
                defaultRegexString = string.Concat(defaultRegexString, "\\.\\d*");
            }

            if (value.IndexOf(' ') > 0)
            {
                defaultRegexString = string.Concat(defaultRegexString, "\\s");
            }

            defaultRegexString = string.Concat(defaultRegexString, "[$|₽|£|¥|R|E|U|Y]");

            return defaultRegexString;
        }
    }
}
