﻿namespace EDU_Money
{
    public enum Currency
    {
        RUB,
        EUR,
        USD,
        JPY
    }
}
