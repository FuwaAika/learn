﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EDU_Money.Extensions
{
    public static class StringExtension
    {
        public static Currency ToCurrency(this string value)
        {
            switch (value)
            {
                case "R":
                case "₽":
                    return Currency.RUB;
                case "E":
                case "£":
                    return Currency.EUR;
                case "U":
                case "$":
                    return Currency.USD;
                case "Y":
                case "¥":
                    return Currency.JPY;
                default:
                    throw new ArgumentOutOfRangeException(nameof(value), value, null);
            }
        }
    }
}
