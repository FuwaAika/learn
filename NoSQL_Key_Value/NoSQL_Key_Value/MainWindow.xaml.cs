﻿using ServiceStack.Redis;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reactive.Subjects;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using HandyControl.Controls;
using Guid = System.Guid;
using MessageBox = System.Windows.MessageBox;
using Window = System.Windows.Window;


namespace NoSQL_Key_Value
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public RedisManagerPool rmp { get; set; }
        public IRedisClient clientDb1 { get; set; }
        public IRedisClient clientDb2 { get; set; }
        private ISubject<string> log { get; set; }
        public MainWindow()
        {
            InitializeComponent();
            //log.Subscribe(l => LogBox.Items.Add(l));
            rmp = new RedisManagerPool("localhost:6379");
            clientDb1 = rmp.GetClient();
            clientDb1.Db = 1;
            clientDb2 = rmp.GetClient();
            clientDb2.Db = 2;

        }

        public BitmapImage GetImage(string searchTag)
        {
            var imageKey = clientDb2.GetValue(searchTag);
            if (imageKey != null)
            {
                var base64 = clientDb1.GetValue(imageKey);
                var image = ConvertBase64ToImage(base64);
                return image;
            }

            return null;
        }
        public string SetImage(string base64)
        {
            var key = Guid.NewGuid().ToString();
            clientDb1.SetValue(key, base64);
            return key;
        }

        public void SetImageSearchString(string key, string imageKey)
        {
            clientDb2.SetValue(key, imageKey);
        }

        public string ConvertImageFileToBase64(string path)
        {
            var bi = new BitmapImage(new Uri(path));
            var encoder = new JpegBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(bi));
            var ms = new MemoryStream();
            encoder.Save(ms);
            var file = ms.ToArray();
            
            return Convert.ToBase64String(file);
        }
        public BitmapImage ConvertBase64ToImage(string base64)
        {
            var file = Convert.FromBase64String(base64);
            
            var bi = new BitmapImage();
            bi.BeginInit();
            bi.StreamSource = new MemoryStream(file);
            bi.EndInit();

            return bi;
        }

        private void AddImageButton_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new OpenFileDialog
            {
                Multiselect = false
            };
            if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                var path = dialog.FileName;
                if (File.Exists(path))
                {
                    var base64 = ConvertImageFileToBase64(path);
                    var imageKey = SetImage(base64);
                    clientDb1.Save();
                    if (!string.IsNullOrEmpty(Tag.Text))
                    {
                        var ze = Tag.Text.Replace("\n", "").Replace("\r", "");
                        SetImageSearchString(Tag.Text.Replace("\n","").Replace("\r",""), imageKey);
                        clientDb2.Save();
                    }
                    //AskForFileTags(imageKey);
                }
                else
                {
                    MessageBox.Show("File Not Exists");
                }
            }
            else
            {
                MessageBox.Show("Select Image");
            }
        }

        private void AskForFileTags(string imageKey)
        {
            //var tags = new List<string>();
            //var tagsDialog = new AskFileTagsDialog();
            //Dialog.Show(tagsDialog);
        }

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(SearchBox.Text))
            {
                var image = GetImage(SearchBox.Text);
                if (image != null)
                {
                    //ImageBox.Source = image;
                    var img = new Image
                    {
                        Source = image, 
                        Height = 364, 
                        Width = 518, 
                        Stretch = Stretch.Uniform
                    };
                    var grid = new Grid
                    {
                        Width = 518, 
                        Height = 364
                    };
                    grid.Children.Add(img);
                    
                    ImagesCarousel.Items.Add(grid);
                }
                else
                {
                    MessageBox.Show("Image not found");
                }
            }
        }
    }
}
