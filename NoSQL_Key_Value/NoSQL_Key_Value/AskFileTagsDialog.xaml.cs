﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace NoSQL_Key_Value
{
    /// <summary>
    /// Логика взаимодействия для AskFileTagsDialog.xaml
    /// </summary>
    public partial class AskFileTagsDialog
    {
        public AskFileTagsDialog()
        {
            InitializeComponent();
            TagsBox.Items.Clear();
            Tag.Text = "";
        }

        private void AddTag_Click(object sender, RoutedEventArgs e)
        {
            TagsBox.Items.Add(Tag.Text);
            Tag.Text = "";
        }
    }
}
